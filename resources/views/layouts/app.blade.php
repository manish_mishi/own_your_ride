<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Own your ride brings you to own something which helps to you to get ride when your eyes searches for some quick assist.">
    <meta name="keywords" content="Goa,Bikes on rent,Rent,Rental Bikes,Bike,Bikes,Goa on Bike,Baga,Anjuna,Calingute,Panjim,trip on bike,Goa trips,Goa trips on bike,Goa hotel,hotels in goa,South Goa,North Goa">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}" >
    <link rel="stylesheet" href="{{ url('assets/css/typeahead.css') }}" >
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
    @import url(http://fonts.googleapis.com/css?family=Montserrat:400,700|Handlee);
    body {
       font-family: 'Lato', Calibri, Arial, sans-serif;
       background: #ddd url('own_your_ride.jpg') no-repeat top left;
       font-weight: 300;
       font-size: 15px;
       color: #333;
       -webkit-font-smoothing: antialiased;
       overflow-y: scroll;
       overflow-x: hidden; 
   }

   .fa-btn {
    margin-right: 6px;
}

</style>

  <!-- <link rel="stylesheet" href="{{url('css/bootstrap-datetimepicker.min.css')}}" /> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />

<!-- JavaScripts -->
    
    <script src="{{url('assets/js/jquery-1.11.0.min.js')}}" crossorigin="anonymous"></script>
    <script src="{{url('assets/js/bootstrap.min.js')}}" crossorigin="anonymous"></script>
    <!-- <script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.min.js')}}"></script> -->
    <script type="text/javascript" src="{{url('assets/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
</head>
<body id="app-layout">
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Own Your Ride
                </a>
            </div>

        </div>
    </nav>

    @yield('content')

    
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
