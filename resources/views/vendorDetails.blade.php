@extends('layouts.header')

@section('title')
Details
@endsection

@section('content')

<link rel="stylesheet" type="text/css" href="{{url('assets/css/getBox.css')}}">

<style type="text/css">

@import url(http://fonts.googleapis.com/css?family=Montserrat:400,700|Handlee);
body {
	font-family: 'Lato', Calibri, Arial, sans-serif;
	font-weight: 300;
	font-size: 15px;
	color: #333;
	background-color: #ABDCD6;
	-webkit-font-smoothing: antialiased;
	overflow-y: scroll;
	overflow-x: hidden; 
}
#wrapper {
	padding-left: 250px;
	transition: all 0.4s ease 0s;
}

#sidebar-wrapper {
	margin-left: -250px;
	left: 250px;
	width: 250px;
	/*background: #f5f5f5;*/
	position: fixed;
	height: 100%;
	overflow-y: auto;
	z-index: 1000;
	transition: all 0.4s ease 0s;
}

#wrapper.active {
	padding-left: 0;
}

#wrapper.active #sidebar-wrapper {
	left: 0;
}

#page-content-wrapper {
	width: 100%;
}



.sidebar-nav {
	position: absolute;
	top: 0;
	width: 250px;
	list-style: none;
	margin: 0;
	padding: 0;
}

.sidebar-nav li {
	line-height: 40px;
	text-indent: 20px;
}

.sidebar-nav li a {
	color: #333;
	display: block;
	text-decoration: none;
	padding-left: 60px;
}

.sidebar-nav li a span:before {
	position: absolute;
	left: 0;
	color: #41484c;
	text-align: center;
	width: 20px;
	line-height: 18px;
}

.sidebar-nav li a:hover,
.sidebar-nav li.active {
	color: #000;
	background: rgba(255,255,255,0.2);
	text-decoration: none;
}

.sidebar-nav li a:active,
.sidebar-nav li a:focus {
	text-decoration: none;
}

.sidebar-nav > .sidebar-brand {
	height: 65px;
	line-height: 60px;
	font-size: 18px;
}

.sidebar-nav > .sidebar-brand a {
	color: #333;
}

.sidebar-nav > .sidebar-brand a:hover {
	color: #000;
	background: none;
}



.content-header {
	height: 65px;
	line-height: 65px;
}

.content-header h1 {
	margin: 0;
	margin-left: 20px;
	line-height: 65px;
	display: inline-block;
}

#menu-toggle {
	text-decoration: none;
}

.btn-menu {
	color: #000;
} 

.inset {
	padding: 20px;
}

@media (max-width:767px) {

	#wrapper {
		padding-left: 0;
	}

	#sidebar-wrapper {
		left: 0;
	}

	#wrapper.active {
		position: relative;
		left: 250px;
	}

	#wrapper.active #sidebar-wrapper {
		left: 250px;
		width: 250px;
		transition: all 0.4s ease 0s;
	}

	#menu-toggle {
		display: inline-block;
	}

	.inset {
		padding: 15px;
	}

}

</style>

<div id="wrapper">
	<div id="sidebar-wrapper">

		<div class="row">			
			<aside class="column left_column col-xs-12 col-sm-3">
				<nav id="spy">
					<ul class="sidebar-nav nav">
						<li class="sidebar-brand">
							<a href="{{url('/')}}"><span class="fa fa-home solo">Home</span></a>
						</li>
						<li>
							<a href="#blogs" data-scroll>
								<span class="fa fa-anchor solo">Blogs</span>
							</a>
						</li>
						<li>
							<a href="#joinus" data-scroll>
								<span class="fa fa-anchor solo">Join Us</span>
							</a>
						</li>
						<li>
							<a href="#contactus" data-scroll>
								<span class="fa fa-anchor solo">Contact Us</span>
							</a>
						</li>
						<li>
							<a href="#aboutus" data-scroll>
								<span class="fa fa-anchor solo">About Us</span>
							</a>
						</li>
					</ul>
				</nav>
			</aside>
		</div>

	</div>

	<ol class="breadcrumb">
		<li><a href="{{ url('/') }}">Home</a></li>
		<li class="active">Vendor Details</li>
	</ol>

	<ul class="nav">
		@foreach($vendorDetails as $key => $vendor)
		<li>
			<div class="dashboard-box dashboard-box-chart bg-white content-box">
				<h3>{{$vendor->name}}</h3>

				<div class="content-wrapper">
					<label>{{$vendor->other_details}}</label>
					<br>
					<label><u><i>{{$vendor->location}}</i></u></label>
				</div>

				<div class="button-pane">
					<a class="float-left btn" id="getContact<?=$key?>">
						Get Contact Details
					</a>
					<br>
					<label style="display:none" id="contact<?=$key?>">{{$vendor->contact}}</label>
					<input type="hidden" value="{{$vendor->id}}" id="vendorId<?=$key?>">
				</div>
			</div>
		</li>

		<script type="text/javascript">
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$("#getContact<?=$key?>").click(function () {
			if(document.getElementById("contact<?=$key?>").style.display == "none")
			{
				document.getElementById("contact<?=$key?>").style.display="block";

				$vendorId = $("#vendorId<?=$key?>").val();

				$.ajax({
					method: "POST",
					url: '{{url("vendor/checked_contacts")}}'+"/"+$vendorId,
					success : function(data){
						console.log("Successfully Done!");

					}

				});
			}
			else
			{
				document.getElementById("contact<?=$key?>").style.display="none";
			}
		});
		</script>
		@endforeach
	</ul>
</div>

<script type="text/javascript">

/*Menu-toggle*/
  /*  $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");
    });*/

/*Scroll Spy*/
   /* $('body').scrollspy({ target: '#spy', offset:80});
   */
   /*Smooth link animation*/
/*    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });*/
</script>

@endsection