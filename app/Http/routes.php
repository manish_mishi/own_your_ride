<?php

/*
|--------------------------------------------------------------------------
| Application Routes  B@}(*fQm7R_o
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::post('vendors', 'VendorController@allDetails');

/*Route::auth();

Route::get('/home', 'HomeController@index');*/


/*  Ajax Function   */

Route::post('vendor/checked_contacts/{vendorId}','VendorController@addCheckedCount');

