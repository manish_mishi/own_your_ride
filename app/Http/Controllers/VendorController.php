<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\VendorContact;

class VendorController extends Controller
{
	public function allDetails(Request $request)
	{
		$location = $request->get('location');

		$vendorDetails = DB::table('vendor_list')
		->where('location',$location)
		->get();

		if($vendorDetails == null)
		{
			$request->session()->flash('alert-danger', 'Location not found,Try again!');

			return redirect()->back();
		}

		return view('vendorDetails',compact('vendorDetails'));
	}



 //Ajax

	public function addCheckedCount($vendorId)
	{
		$vendorCheckedDetails = DB::table('checked_contact_vendor')
		->where('vendor_id',$vendorId)
		->get();

		if($vendorCheckedDetails == null)
		{
				$vendorcontact = new VendorContact;
				$vendorcontact->count="1";
				$vendorcontact->vendor_id=$vendorId;
				$vendorcontact->save();
		}
		else
		{
			$prevCount = $vendorCheckedDetails[0]->count;
			
				DB::table('checked_contact_vendor')
				->where('vendor_id', $vendorId)
				->update(['count' => $prevCount+1]);
		}

		return;

	}
}
