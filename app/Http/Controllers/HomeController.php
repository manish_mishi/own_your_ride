<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendorList = DB::table('vendor_list')
        ->select('*')
        ->get();

        $_tmp = array();
        foreach($vendorList as $key => $value) {
            if(!array_key_exists($value->location,$_tmp)) {
                $_tmp [$value->location] = $value;
            }
        }
        $vendorList = array_values($_tmp);
        
        return view('home',compact('vendorList'));
    }
}
